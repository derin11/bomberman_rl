from collections import namedtuple, deque
from hashlib import new
from pyexpat import features
import numpy as np

import pickle
from typing import List
from agent_code.agent_007_q_table.callbacks import get_state_index

import events as e
from .callbacks import state_to_features, get_state_index, check_for_opponents, danger
import settings as s

ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']
ACTION_TO_INT = {'UP':0, 'RIGHT' : 1 , 'DOWN': 2, 'LEFT': 3, 'WAIT':4, 'BOMB': 5}

EpisodeTransition = namedtuple('EpisodeTransition',
                        ('state', 'action', 'next_state', 'reward', 'timestep'))                    

# Hyper parameters 
GAMMA = 0.9 # discount factor
LEARNING_RATE = 0.07

def setup_training(self):
    """
    Initialise self for training purpose.

    This is called after `setup` in callbacks.py.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """

    #save total rewards for each game
    self.total_rewards = 0

    # Example: Setup an array that will note transition tuples
    self.episode_transitions = deque(maxlen=s.MAX_STEPS)


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    """
    Called once per step to allow intermediate rewards based on game events.

    When this method is called, self.events will contain a list of all game
    events relevant to your agent that occurred during the previous step. Consult
    settings.py to see what events are tracked. You can hand out rewards to your
    agent based on these events and your knowledge of the (new) game state.

    This is *one* of the places where you could update your agent.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    :param old_game_state: The state that was passed to the last call of `act`.
    :param self_action: The action that you took.
    :param new_game_state: The state the agent is in now.
    :param events: The events that occurred when going from  `old_game_state` to `new_game_state`
    """
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    #In the first game state our agent can not achieve anything so we ingore it
    if old_game_state == None:
        return
        
    #Append custom events for rewards
    total_events = append_custom_events(self, old_game_state, new_game_state, events)

    #calculate total reward for this timestep
    step_reward = reward_from_events(self, total_events)
    self.total_rewards = self.total_rewards + step_reward

    # state_to_features is defined in callbacks.py
    # fill experience buffer with transitions
    self.episode_transitions.append(EpisodeTransition(state_to_features(self, old_game_state),self_action, state_to_features(self, new_game_state), step_reward, old_game_state['step']))


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    """
    Called at the end of each game or when the agent died to hand out final rewards.
    This replaces game_events_occurred in this round.

    This is similar to game_events_occurred. self.events will contain all events that
    occurred during your agent's final step.

    This is *one* of the places where you could update your agent.
    This is also a good place to store an agent that you updated.

    :param self: The same object that is passed to all of your callbacks.
    """
    self.logger.debug(f'Encountered event(s) {", ".join(map(repr, events))} in final step')

    #calculate total reward for this timestep
    step_reward = reward_from_events(self, events)
    self.total_rewards = self.total_rewards + step_reward

    #update the Q-table at the end of the round by accessing the episode transitions
    for i in range(len(self.episode_transitions)-1):
        old_state = self.episode_transitions[i][0]
        new_state = self.episode_transitions[i+1][0]

        old_action = self.episode_transitions[i][1]
        new_action = self.episode_transitions[i+1][1]

        #get the state indices of the old and the new state
        index_1 = get_state_index(old_state)
        index_2 = get_state_index(new_state)

        #update Q table
        self.q_table[index_1][ACTION_TO_INT[old_action]] =  self.q_table[index_1][ACTION_TO_INT[old_action]] + LEARNING_RATE * (self.episode_transitions[i][3] + GAMMA * self.q_table[index_2][ACTION_TO_INT[new_action]] -  self.q_table[index_1][ACTION_TO_INT[old_action]])
    
    #update the Q-value for the last state-action combination
    last_state = state_to_features(self,last_game_state)
    index = get_state_index(last_state)
    self.q_table[index][ACTION_TO_INT[last_action]] =  self.q_table[index][ACTION_TO_INT[last_action]] + LEARNING_RATE * (step_reward  -  self.q_table[index][ACTION_TO_INT[last_action]])
    
    #reset total rewards per game back to 0
    #clear episode_rewards and episode_transitions
    print("total rewards: ",self.total_rewards)

    self.total_rewards = 0
    self.episode_transitions.clear()

    if last_game_state["round"] % 500 == 0:
        print("Save to file")
        # Store the model
        with open("my-saved-model.pt", "wb") as file:
            pickle.dump(self.q_table, file)

def reward_from_events(self, events: List[str]) -> int:
    """
    *This is not a required function, but an idea to structure your code.*

    Here you can modify the rewards your agent get so as to en/discourage
    certain behavior.
    """

    game_rewards = {
        e.INVALID_ACTION: -20,
        e.GOT_KILLED: -500,
        e.COIN_COLLECTED: 20,
        e.OPPONENT_ELIMINATED: 80,
        'LIFE_SAVING_MOVE': 20,
        'GOOD_BOMB_PLACEMENT': 10,
        'BAD_BOMB_PLACEMENT': -50,
        'DEADLY_MOVE': -150,
        'MOVES_TOWARD_TARGET': 5,
        'WAITING_ONLY_OPTION': 10,
        'BAD_MOVE': -4,
        e.CRATE_DESTROYED: 10
    }
    reward_sum = 0
    for event in events:
        if event in game_rewards:
            #print(events)
            reward_sum += game_rewards[event]
    self.logger.info(f"Awarded {reward_sum} for events {', '.join(events)}")

    #print(reward_sum)
    return reward_sum

def append_custom_events(self,old_game_state: dict, new_game_state: dict, events: List[str]) -> List[str]:

    """
    Appends all our custom events to the events list
    so we can calculate the total rewards out of these

    Args:
        events (List[str]): The non custom events, that happened between two game states
        old_game_state: dict: The game state before the events happened
        new_game_state: dict: The game state after the events happened

    Returns:
        List[str]: Full event list with custom events appended
    """

    if e.INVALID_ACTION in events:
        return events
    
    features = state_to_features(self,old_game_state)
    _, _, _, (x,y) = old_game_state['self']
    field = np.array(old_game_state['field'])
    coins = np.array(old_game_state['coins'])
    bombs = old_game_state['bombs']
    explosion_map = np.array(old_game_state['explosion_map'])
    others = old_game_state['others']
    others_position = np.zeros( (len(others), 2), dtype=int)
    explosion_indices = np.array(np.where(explosion_map > 0)).T
    explosion_around_agent = np.array([ int(explosion_map[x-1,y]!= 0), int(explosion_map[x+1,y]!= 0), int(explosion_map[x,y-1]!=0), int(explosion_map[x,y+1] != 0)  ])

    for i,opponent in enumerate(others):
        others_position[i] = opponent[3]

    danger_left, danger_right, danger_up, danger_down, danger_wait = features[12:17]
    
    dangerous_movement = [danger_left, danger_right, danger_up, danger_down]

    #check, if waiting is dangerous we need to move 
    if danger_wait == 1: 
        #check if did a life saving move
        if danger_left == 0 and e.MOVED_LEFT in events:
            events.append("LIFE_SAVING_MOVE")
        elif danger_right == 0 and e.MOVED_RIGHT in events:
            events.append("LIFE_SAVING_MOVE")
        elif danger_up == 0 and e.MOVED_UP in events:
            events.append("LIFE_SAVING_MOVE")
        elif danger_down == 0 and e.MOVED_DOWN in events:
            events.append("LIFE_SAVING_MOVE")
        else: 
            events.append("DEADLY_MOVE")

    elif e.BOMB_DROPPED in events:
        #check if dropped the bomb correctly
        updated_bombs = bombs.copy()
        updated_bombs.append(((x,y), 4))
        if not danger(self, np.array([x,y]), updated_bombs.copy(), field.copy(), explosion_map.copy(), others_position.copy()): 
            if check_for_crates(self, np.array([x,y]), field.copy()) or check_for_opponents(self, np.array([x,y]), field.copy(), others_position):
                events.append("GOOD_BOMB_PLACEMENT")
            else:
                events.append("BAD_BOMB_PLACEMENT")
        else:
            events.append("DEADLY_MOVE")
    else:
        valid_list = np.array([ int(field[x-1,y] == 0),  int(field[x+1,y] == 0) , int(field[x,y-1] == 0) , int(field[x,y+1] == 0) ] )
        crates_around = np.array([ int(field[x-1,y] == 1),  int(field[x+1,y] == 1) , int(field[x,y-1] == 1) , int(field[x,y+1] == 1) ] )
        valid_list[ np.where( np.logical_or(dangerous_movement == 1, explosion_around_agent == 1) ) ] = 0

        explosion_left , explosion_right, explosion_up, explosion_down = explosion_around_agent
        target_left , target_right, target_up, target_down = features[0:4]
        target_array = [target_left,target_right, target_up, target_down]

        opponent_directions = features[18:22]

        if np.all(valid_list == 0) and e.WAITED in events:
            events.append("WAITING_ONLY_OPTION")
        
        #check if performed a deadly move 
        #->bomb
        elif (danger_left == 1 and e.MOVED_LEFT in events) or (danger_right == 1 and e.MOVED_RIGHT in events) or (danger_up == 1 and e.MOVED_UP in events) or (danger_down == 1 and e.MOVED_DOWN in events) or (danger_wait == 1 and e.WAITED in events):
            events.append("DEADLY_MOVE")
        #->eyplosion
        elif (explosion_left == 1 and e.MOVED_LEFT in events) or (explosion_right == 1 and e.MOVED_RIGHT in events) or (explosion_up== 1 and e.MOVED_UP in events) or (explosion_down== 1 and e.MOVED_DOWN in events):
            events.append("DEADLY_MOVE")

        elif np.any(opponent_directions==-1):
            if (e.MOVED_LEFT in events and opponent_directions[0] == -1) or (e.MOVED_RIGHT in events and opponent_directions[1] == -1) or (e.MOVED_UP in events and opponent_directions[2] == -1) or (e.MOVED_DOWN in events and opponent_directions[3] == -1):
                events.append("MOVES_TOWARD_TARGET")
            else:
                events.append("BAD_MOVE")  
        else:
            if (e.MOVED_LEFT in events and target_left == 1) or (e.MOVED_RIGHT in events and target_right == 1) or (e.MOVED_UP in events and target_up == 1) or (e.MOVED_DOWN in events and target_down == 1) and np.all(crates_around == 0):
                events.append("MOVES_TOWARD_TARGET")
            elif np.any(np.logical_and(target_array==valid_list, target_array==1)):
                events.append("BAD_MOVE")         

    return events


def check_for_crates(self, agent_position, field):
    """Given a field and an agent_position, this function checks if any crates would be destroyed if 
    a bomb was to be planted at that agent_position

    Args:
        agent_position: The position of the agent, where we want to check if a bomb would hit any crates
        field: The current field including walls etc.

    Returns:
        Bool whether planting a bomb at the given position would destroy any crates or not
    """
    x, y = agent_position
    
    #field == 1 indicates a crate, -1 indicates a wall
    #we can break when we hit a wall, since this will block the further explosion
    #check if the bomb would hit a crate on the right side of the agent
    for i in range(1,4):
        if field[x+i][y] == -1:
            break 

        if field[x+i,y] == 1:
            return True

    #check if the bomb would hit a crate on the left side of the agent
    for i in range(1,4):
        if field[x-i][y] == -1:
            break

        if field[x-i,y] == 1:
            return True 

    #check if the bomb would hit a crate below the agent
    for i in range(1,4):
        if field[x][y+i] == -1:
            break

        if field[x,y+i] == 1:
            return True 

    #check if the bomb would hit a crate above the agent
    for i in range(1,4):
        if field[x][y-i] == -1:
            break 
        
        if field[x,y-i] == 1:
            return True 

    #no crates hit
    return False


def check_near_bombs(self, agent_position, field, bombs, steps_passed):
    """Given a field, an agent_position and positions where bombs are located
    as well as their cooldown (and how many steps we look into the future, which will decrease their colldown)
    this function checks if the agent positions is within
    the explosion radius of one of the bombs and if so how long this bomb will take
    to explode

    This function is used as a helping function for the danger function

    Args:
        agent_position: The position of the agent, where we want to check if a bomb would hit any opponents
        field: The current field including walls etc.
        bombs: Positions where bombs are located
        steps_passed: The amount of steps we are looking into the future, which will decrease bomb_cooldown

    Returns:
        Bool indicating wheter the agent_position is in range of a possible bomb explosion
        and how long this bomb will take to explode
    """

    x, y = agent_position

    #update the field so that the bomb positions are included (indicated by values over 10)
    #the field will contain the cooldown of the bomb + 10
    for bomb in bombs:
        field[bomb[0][0], bomb[0][1]] = 10 + bomb[1] - steps_passed

    #we want to find the bomb, that would hit the agent with the smallest cooldown
    #since this is the bomb that put him in danger the soonest
    bomb_found = False
    min_cooldown = 100

    #check if a bomb is on the field of the agent
    if field[x,y] >= 10:
        bomb_found = True
        min_cooldown = min(min_cooldown, field[x,y] - 10)

    #check if a bomb is to the right of the agent, that will hit him if he stays at that position
    #when min_cooldown is 0 this means we have already found a bomb with the minimum possible cooldown
    #so it makes no sense to continue searching
    if min_cooldown != 0:
        for i in range(1,4):
            if field[x+i][y] == -1:
                break 

            if field[x+i,y] >= 10:
                bomb_found = True
                min_cooldown = min(min_cooldown, field[x+i,y] - 10)
                
    #check if a bomb is to the left of the agent, that will hit him if he stays at that position
    if min_cooldown != 0:
        for i in range(1,4):
            if field[x-i][y] == -1:
                break

            if field[x-i,y] >= 10:
                bomb_found = True
                min_cooldown = min(min_cooldown, field[x-i,y] - 10)

    #check if a bomb is below the agent, that will hit him if he stays at that position
    if min_cooldown != 0:
        for i in range(1,4):
            if field[x][y+i] == -1:
                break

            if field[x,y+i] >= 10:
                bomb_found = True
                min_cooldown = min(min_cooldown, field[x,y+i] - 10)

    #check if a bomb is above of the agent, that will hit him if he stays at that position
    if min_cooldown != 0:
        for i in range(1,4):
            if field[x][y-i] == -1:
                break 
            
            if field[x,y-i] >= 10:
                bomb_found = True
                min_cooldown = min(min_cooldown, field[x,y-i] - 10) 

    return bomb_found, min_cooldown

